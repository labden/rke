rke
===

```bash
vagrant up
ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook playbook.yml -i inventory -l rke

wget https://github.com/rancher/rke/releases/download/v1.0.0/rke_linux-amd64
./rke_linux-amd64 up --config cluster.yml

# https://rancher.com/docs/rancher/v2.x/en/troubleshooting/networking/
. kubectl.sh create -f ds-overlaytest.yml
. overlay_test.sh

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key -out crt
docker-compose up -d

echo "rke.local" >> /etc/hosts
```
